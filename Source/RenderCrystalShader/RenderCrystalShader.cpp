// Copyright Epic Games, Inc. All Rights Reserved.

#include "RenderCrystalShader.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, RenderCrystalShader, "RenderCrystalShader" );

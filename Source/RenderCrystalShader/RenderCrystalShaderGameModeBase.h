// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RenderCrystalShaderGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class RENDERCRYSTALSHADER_API ARenderCrystalShaderGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
